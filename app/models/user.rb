class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  belongs_to :plan
  has_one :profile
  
  attr_accessor :stripe_card_token
  def save_with_payment
    if valid?
     #customer = Stripe::Customer.create(description: email, plan: plan_id, source: stripe_card_token) 
     customer = Stripe::Customer.create(description: email, plan: plan_id, source: "tok_17E01wFdrePgxD3LOBdkYar3") 
     self.stripe_cutstomer_token = customer.id
     save!
    end
  end
end
